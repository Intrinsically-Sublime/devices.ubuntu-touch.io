---
name: "Xiaomi Redmi Note 3 (kenzo)"
deviceType: "phone"
enableMdRendering: true
portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "-"
      - id: "torchlight"
        value: "+"
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "-"
      - id: "photo"
        value: "-"
      - id: "video"
        value: "-"
      - id: "switchCamera"
        value: "-"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "-"
      - id: "dataConnection"
        value: "-"
      - id: "dualSim"
        value: "-"
      - id: "calls"
        value: "-"
      - id: "mms"
        value: "-"
      - id: "pinUnlock"
        value: "-"
      - id: "sms"
        value: "-"
      - id: "audioRoutings"
        value: "-"
      - id: "voiceCall"
        value: "-"
      - id: "volumeControl"
        value: "-"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "+"
      - id: "noRebootTest"
        value: "?"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "+"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "+"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "?"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "-"
      - id: "factoryReset"
        value: "?"
      - id: "rtcTime"
        value: "+"
      - id: "sdCard"
        value: "+"
      - id: "shutdown"
        value: "+"
      - id: "wirelessCharging"
        value: "?"
      - id: "wirelessExternalMonitor"
        value: "?"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "-"
      - id: "flightMode"
        value: "+"
      - id: "hotspot"
        value: "-"
      - id: "nfc"
        value: "-"
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "-"
      - id: "fingerprint"
        value: "-"
      - id: "gps"
        value: "?"
      - id: "proximity"
        value: "-"
      - id: "rotation"
        value: "-"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "-"
      - id: "adb"
        value: "-"
      - id: "wiredExternalMonitor"
        value: "-"
deviceInfo:
  - id: "cpu"
    value: "Hexa-core 64-bit"
  - id: "chipset"
    value: "Qualcomm MSM8956 Snapdragon 650"
  - id: "gpu"
    value: "Qualcomm Adreno 510"
  - id: "rom"
    value: "32/64GB"
  - id: "ram"
    value: "3GB"
  - id: "android"
    value: "Android 6.0.1"
  - id: "battery"
    value: "4000 mAh"
  - id: "display"
    value: "1080x1920 pixels, 5.5 in"
  - id: "rearCamera"
    value: "16 MP"
  - id: "frontCamera"
    value: "5 MP"
contributors:
  - name: "mathew-dennis"
    forum: "https://github.com/mathew-dennis"
externalLinks:
  - name: "Telegram - @ubports"
    link: "https://t.me/joinchat/ubports"
    icon: "telegram"
  #- name: "Device Subforum"
  #  link: "https://forums.ubports.com/category/50/oneplus-one"
  #  icon: "yumi"
  #- name: "Report a bug"
  #  link: "https://github.com/ubports/ubuntu-touch/issues"
  #  icon: "github"
---

### Known limitations

#### Wi-Fi

A script to auto enable wifi is included with the build but need to be given executable permission
Reboot to recovery and connect to pc via adb

```
adb shell
mkdir /b
mount /data/system.img /b
chmod +x /b/halium/etc/rc.local
umount /b
sync

```
#### oFono and sensors 

Trying to find a solution to fix these two.

### Bluetooth patch

Is in progress will update once complete.

#### More Details on Fixes

Can be found [here] (https://forum.xda-developers.com/t/ubuntu-touch-linux-alpha-for-kenzo.4288907/)

### Installation

The port is not ready to use the installer, yet :)

#### Fetch files and tools

Download and install the following tools:
- [Halium-install](https://gitlab.com/JBBgameich/halium-install/)


Download the following files:
- [halium-boot.img](https://drive.google.com/file/d/1E9zbyNfcNRq-jXPPbL1z5q0_iPPW0bdT/view)
- [system.img](https://drive.google.com/file/d/1qxHtbJsir6rcDW2JTgvsWr8Ja9_tSABK/view)
- [ubports rootfs](https://ci.ubports.com/job/xenial-hybris-rootfs-arm64/)

#### Unlock OEM installation

In Android activate developper mode and use MI unlock tool to do an oem unlock (skip this step if you are on a custom rom)

If this step is not done you willnot be able to flash hlium-boot.

#### Install TWRP or Orange fox recovery

Install the TWRP recovery as instructed in its [documentation](https://twrp.me/xiaomi/xiaomiredminote3.html).
or install Orange fox recovery from[here](https://orangefox.download/device/kenzo)

#### Reboot to recovery and format data

1. From the computer enter `adb reboot recovery`.
1. On your phone go to `wipe`->`Format Data` then enter `yes` as instructed.
1. From the PC Flash `system.img` using the halium-install  script `./halium-install -p ut ubuntu-touch-hybris-xenial-arm64-rootfs.tar.gz system.img`.
1. Flash `halium-boot` from pc using fastboot  ` fastboot flash boot halium-boot.img`.
1. Wait, after the reboot, you should have Ubuntu Touch Running on your Kenzo  :-)
