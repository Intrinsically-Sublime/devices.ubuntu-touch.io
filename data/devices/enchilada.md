---
name: "OnePlus 6/6T"
comment: "community device"
deviceType: "phone"
enableMdRendering: true
portType: 'Halium 9.0'

aliases:
  - "fajita"

portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "x"
      - id: "torchlight"
        value: "+"
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "+"
      - id: "photo"
        value: "+"
      - id: "video"
        value: "+"
      - id: "switchCamera"
        value: "+"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "+"
      - id: "dualSim"
        value: "+"
      - id: "calls"
        value: "+"
      - id: "mms"
        value: "+"
      - id: "pinUnlock"
        value: "+"
      - id: "sms"
        value: "+"
      - id: "audioRoutings"
        value: "+"
      - id: "voiceCall"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "-"
      - id: "noRebootTest"
        value: "+"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "+"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "+"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "-"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "+"
      - id: "factoryReset"
        value: "+"
      - id: "rtcTime"
        value: "+"
      - id: "sdCard"
        value: "x"
      - id: "shutdown"
        value: "+"
      - id: "wirelessCharging"
        value: "x"
      - id: "wirelessExternalMonitor"
        value: "-"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "+"
      - id: "flightMode"
        value: "+"
      - id: "hotspot"
        value: "+"
      - id: "nfc"
        value: "+"
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "+"
      - id: "fingerprint"
        value: "-"
      - id: "gps"
        value: "+"
      - id: "proximity"
        value: "+"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "+"
      - id: "adb"
        value: "+"
      - id: "wiredExternalMonitor"
        value: "x"

deviceInfo:
  - id: 'cpu'
    value: 'Octa-core (4x 2.8 GHz Kryo 385 Gold & 4x 1.7 GHz Kryo 385 Silver)'
  - id: 'chipset'
    value: 'Qualcomm SDM845 Snapdragon 845'
  - id: 'gpu'
    value: 'Adreno 630'
  - id: 'rom'
    value: '128/256 GB UFS2.1'
  - id: 'ram'
    value: '6/8 GB LPDDR4X'
  - id: 'android'
    value: 'OxygenOS 9 (Android 9)'
  - id: 'battery'
    value: 'Non-removable Li-Po 3400 / 3700 mAh'
  - id: 'display'
    value: '1080x2200 pixels, 6.28 in / 1080x2340 pixel, 6.41 in'
  - id: 'rearCamera'
    value: '16 MP'
  - id: 'frontCamera'
    value: '16 MP'
  - id: 'arch'
    value: 'arm64'
  - id: 'dimensions'
    value: '155.70 x 75.40 x 7.75 / 157.50 x 74.80 x 8.20'
  - id: 'weight'
    value: '177 g / 185 g'
  

contributors:
  - name: 'calebccff'
    photo: ''
    forum: '#'
  - name: 'MrCyjanek'
    photo: ''
    forum: '#'
  - name: 'SevralT'
    photo: ''
    forum: '#'

externalLinks:
  - name: 'Kernel sources'
    link: 'https://gitlab.com/ubports/community-ports/android9/oneplus-6/kernel-oneplus-sdm845'
    icon: 'github'
  - name: 'Device overlay'
    link: 'https://gitlab.com/ubports/community-ports/android9/oneplus-6/oneplus-enchilada-fajita'
    icon: 'github'
  - name: 'CI Builds'
    link: 'https://gitlab.com/ubports/community-ports/android9/oneplus-6/oneplus-enchilada-fajita/-/pipelines'
    icon: 'github'

seo:
  description: 'Switch your Oneplus 6 or 6T to Ubuntu Touch, as your open source daily driver OS.'
  keywords: 'Ubuntu Touch, Oneplus 6, Oneplus 6T, Linux on Mobile'
---

## Important Notes
Apart from the features listed above, there are few things to note:  
<br /> 

1. Single SIM in Slot 2 is stable. However calling from Slot 1 causes the call to fail. Mobile data can work from both slots. However, calls are only stable from Slot 2. If you want to use Ubuntu Touch as your daily driver OS, we recomemd you to move your SIM to Slot 2!

2. Idle drain will be much higher than Android as due to a kernel bug we have to keep `CONFIG_PM_AUTOSLEEP` disabled to prevent a crash when entering idle. Expect 8-12 hours of usable battery life.
