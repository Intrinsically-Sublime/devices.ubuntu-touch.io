---
name: "Google Pixel and Pixel XL"
deviceType: "phone"
portType: 'Halium 9.0'
image: "https://wiki.lineageos.org/images/devices/marlin.png"
maturity: .7
aliases:
  - "sailfish"
externalLinks:
  - name: 'Forum Post'
    link: 'https://forums.ubports.com/category/66/google-pixel-and-xl'
    icon: 'yumi'
---

## Known issues
- No sound in calls
- Fingerprint sensor is wonky
- Bluetooth cannot be turned off
- GPS does not work
