---
name: "Samsung Galaxy S7 (herolte)"
deviceType: "phone"
enableMdRendering: true
portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "-"
      - id: "torchlight"
        value: "+"
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "-"
      - id: "photo"
        value: "-"
      - id: "video"
        value: "-"
      - id: "switchCamera"
        value: "-"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "+"
      - id: "calls"
        value: "+"
      - id: "mms"
        value: "+"
      - id: "pinUnlock"
        value: "+"
      - id: "sms"
        value: "+"
      - id: "audioRoutings"
        value: "+"
      - id: "voiceCall"
        value: "+"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "+"
      - id: "noRebootTest"
        value: "?"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "?"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "-"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "?"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "-"
      - id: "factoryReset"
        value: "?"
      - id: "rtcTime"
        value: "?"
      - id: "sdCard"
        value: "-"
      - id: "shutdown"
        value: "-"
      - id: "wirelessCharging"
        value: "?"
      - id: "wirelessExternalMonitor"
        value: "?"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "-"
      - id: "flightMode"
        value: "+"
      - id: "hotspot"
        value: "-"
      - id: "nfc"
        value: "-"
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "-"
      - id: "fingerprint"
        value: "-"
      - id: "gps"
        value: "?"
      - id: "proximity"
        value: "+"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "-"
      - id: "adb"
        value: "?"
deviceInfo:
  - id: "cpu"
    value: "2.3GHz Quad-Core (Custom Core) + 1.6GHz Quad-Core (Cortex®-A53)"
  - id: "chipset"
    value: "Samsung Exynos 8890"
  - id: "gpu"
    value: "Mali™-T880 MP12"
  - id: "rom"
    value: "16/32GB"
  - id: "ram"
    value: "4GB"
  - id: "android"
    value: "8.0"
  - id: "battery"
    value: "3000 mAh"
  - id: "display"
    value: "2560x1440 pixels, 5.1 in"
  - id: "rearCamera"
    value: "12 MP"
  - id: "frontCamera"
    value: "5 MP"
contributors:
  - name: ZeroPointEnergy
    forum: https://forums.ubports.com/user/zeropointenergy
  - name: Aribk
    forum: https://forums.ubports.com/user/aribk
  - name: Oliv
    forum: https://forums.ubports.com/user/oliv
externalLinks:
  - name: "Telegram - @ubports"
    link: "https://t.me/joinchat/ubports"
    icon: "telegram"
  #- name: "Device Subforum"
  #  link: "https://forums.ubports.com/category/50/oneplus-one"
  #  icon: "yumi"
  #- name: "Report a bug"
  #  link: "https://github.com/ubports/ubuntu-touch/issues"
  #  icon: "github"
---

### Known limitations
#### Audio calls
While they work (headphones, speaker, loudspeaker), volume can not be changed.

#### oFono
Current oFono needs to be patched to work with the herolte.
A precompiled binary can be downloaded [here](https://links.iopush.net/herolte_ofono).

Installation:
```
# Remount system with write permission
sudo mount -o remount,rw /
# Extract archive and install new oFono version
tar xvf debs_1.18.0+ubports2local~1612885094.tar 
sudo dpkg -i ofono_1.18.0+ubports2local~1612885094_armhf.deb
# Reboot
```

#### Wi-Fi
While Wi-Fi is working there is no reconnection after reboot. This is due to several things:

- Issue with MAC address loader. Partially fixed and not published, need to be reworked as it prevents loading the right calibration file.
- Unknown issue, probably a race condition between Wi-fi driver and Samsung's Macloader

#### MMS
MMS are working but a change needs to be applied until it gets merged to `xenial`
```
sudo ubports-qa install xenial_-_android9
```

#### Torch
Code is not yet merged in the `xenial` branch but it can be activated thanks to:
```
ubports-qa install indicator-power 48
```


### Installation
The port is not ready to use the installer, yet :)

#### Fetch files and tools
Download and install the following tools:
- [Halium-install](https://gitlab.com/JBBgameich/halium-install/)
- [Heimdall](https://gitlab.com/BenjaminDobell/Heimdall)

Download the following files:
- [halium-boot.img](https://links.iopush.net/herolte_haliumboot)
- [system.img](https://links.iopush.net/herolte_system)
- [ubports rootfs](https://ci.ubports.com/job/xenial-rootfs-armhf/lastSuccessfulBuild/artifact/out/ubports-touch.rootfs-xenial-armhf.tar.gz) (only armhf is tested for now)

#### Unlock OEM installation
In Android activate developper mode thanks to [this guide](https://www.androidcentral.com/how-enable-developer-mode-galaxy-s7).  
Then go to Android developer options and activate `enable OEM unlock slider`.

If this step is not done you will likely get the error `Custom binary blocked by FRP lock`
when flashing `halium-boot.img`.

#### Install TWRP
Instal the TWRP recovery as instructed in its [documentation](https://twrp.me/samsung/samsunggalaxys7.html).

#### Reboot to recovery and format data
1. From the computer enter `adb reboot recovery`.
1. Go to `wipe`->`Format Data` the enter `yes` as instructed.
1. Flash `system.img` thanks to `./halium-install -v -i -p ut ubports-touch.rootfs-xenial-armhf.tar.gz system.img`.
1. Flash `halium-boot` thanks to ` ./Heimdall/build/bin/heimdall flash --BOOT halium-boot.img`.
1. Wait, after the reboot, you should have UBports  :-)
1. Some fix might be required, see `Known limitations` chapter.
